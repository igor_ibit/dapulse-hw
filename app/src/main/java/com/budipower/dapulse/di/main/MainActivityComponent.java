package com.budipower.dapulse.di.main;

import com.budipower.dapulse.ActivityScope;
import com.budipower.dapulse.ui.main.MainActivity;

import dagger.Subcomponent;

/**
 * Created by igori on 03/03/2017.
 */

@ActivityScope
@Subcomponent(modules = MainActivityModule.class)
public interface MainActivityComponent {

    MainActivity inject(MainActivity mainActivity);
}
