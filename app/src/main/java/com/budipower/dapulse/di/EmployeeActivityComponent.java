package com.budipower.dapulse.di;

import com.budipower.dapulse.ActivityScope;
import com.budipower.dapulse.ui.employee.EmployeeActivity;

import dagger.Subcomponent;

/**
 * Created by igori on 03/03/2017.
 */

@ActivityScope
@Subcomponent(modules = EmployeeActivityModule.class)
public interface EmployeeActivityComponent {

    EmployeeActivity inject(EmployeeActivity employeeActivity);
}
