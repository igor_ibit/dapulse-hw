package com.budipower.dapulse.di;

import com.budipower.dapulse.di.group.GroupActivityComponent;
import com.budipower.dapulse.di.group.GroupActivityModule;
import com.budipower.dapulse.di.main.MainActivityComponent;
import com.budipower.dapulse.di.main.MainActivityModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by igori on 03/03/2017.
 */
@Singleton
@Component(modules = {
        ApiModule.class,
        DataModule.class,
        HelpersModule.class
}
)
public interface AppComponent {

    MainActivityComponent plus(MainActivityModule mainActivityModule);

    EmployeeActivityComponent plus(EmployeeActivityModule employeeActivityModule);

    GroupActivityComponent plus(GroupActivityModule groupActivityModule);
}
