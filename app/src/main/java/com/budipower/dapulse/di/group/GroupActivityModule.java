package com.budipower.dapulse.di.group;

import com.budipower.dapulse.ActivityScope;
import com.budipower.dapulse.data.DataProvider;
import com.budipower.dapulse.ui.group.GroupActivity;
import com.budipower.dapulse.ui.group.GroupPresenter;
import com.budipower.dapulse.ui.group.GroupPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by igori on 03/03/2017.
 */

@Module
public class GroupActivityModule {

    private GroupActivity groupActivity;

    public GroupActivityModule(GroupActivity groupActivity) {
        this.groupActivity = groupActivity;
    }

    @Provides
    @ActivityScope
    GroupPresenter provideGroupPresenter(DataProvider dataProvider) {
        return new GroupPresenterImpl(groupActivity, dataProvider);
    }
}
