package com.budipower.dapulse.di.group;

import com.budipower.dapulse.ActivityScope;
import com.budipower.dapulse.ui.group.GroupActivity;

import dagger.Subcomponent;

/**
 * Created by igori on 03/03/2017.
 */

@ActivityScope
@Subcomponent(modules = GroupActivityModule.class)
public interface GroupActivityComponent {
    GroupActivity inject(GroupActivity groupActivity);
}
