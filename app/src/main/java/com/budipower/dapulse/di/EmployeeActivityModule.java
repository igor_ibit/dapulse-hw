package com.budipower.dapulse.di;

import com.budipower.dapulse.ActivityScope;
import com.budipower.dapulse.data.DataProvider;
import com.budipower.dapulse.ui.employee.EmployeeActivity;
import com.budipower.dapulse.ui.employee.EmployeePresenter;
import com.budipower.dapulse.ui.employee.EmployeePresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by igori on 03/03/2017.
 */

@Module
public class EmployeeActivityModule {

    private EmployeeActivity employeeActivity;

    public EmployeeActivityModule(EmployeeActivity employeeActivity) {
        this.employeeActivity = employeeActivity;
    }

    @Provides
    @ActivityScope
    EmployeePresenter provideEmployeePresenter(DataProvider dataProvider) {
        return new EmployeePresenterImpl(employeeActivity, dataProvider);
    }
}
