package com.budipower.dapulse.di.main;

import com.budipower.dapulse.ActivityScope;
import com.budipower.dapulse.data.DataProvider;
import com.budipower.dapulse.ui.main.MainActivity;
import com.budipower.dapulse.ui.main.MainPresenter;
import com.budipower.dapulse.ui.main.MainPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by igori on 03/03/2017.
 */

@Module
public class MainActivityModule {

    private MainActivity mainActivity;

    public MainActivityModule(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Provides
    @ActivityScope
    MainPresenter provideMainPresenter(DataProvider dataProvider) {
        return new MainPresenterImpl(dataProvider, mainActivity);
    }
}
