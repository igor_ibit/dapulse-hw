package com.budipower.dapulse.di;

import com.budipower.dapulse.data.DataProvider;
import com.budipower.dapulse.data.EmployeeDataProviderImpl;
import com.budipower.dapulse.helpers.DBHelper;
import com.budipower.dapulse.helpers.NetworkAccessHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by igori on 03/03/2017.
 */

@Module
public class DataModule {

    @Provides
    @Singleton
    public DataProvider getDataProvider(DBHelper dbHelper, NetworkAccessHelper networkHelper) {
        return new EmployeeDataProviderImpl(dbHelper, networkHelper);
    }
}
