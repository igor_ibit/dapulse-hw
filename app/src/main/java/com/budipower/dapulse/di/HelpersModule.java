package com.budipower.dapulse.di;

import com.budipower.dapulse.App;
import com.budipower.dapulse.helpers.DBHelper;
import com.budipower.dapulse.helpers.NetworkAccessHelper;
import com.budipower.dapulse.helpers.NetworkAccessHelperImpl;
import com.budipower.dapulse.helpers.RealmDbHelper;
import com.budipower.dapulse.retrofit.EmployeeApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;

/**
 * Created by igori on 03/03/2017.
 */

@Module
public class HelpersModule {

    private App app;

    public HelpersModule(App app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public DBHelper getDbHelper() {
        Realm.init(app);
        return new RealmDbHelper();
    }

    @Provides
    @Singleton
    public NetworkAccessHelper getNetworkAccessHelper(DBHelper dbHelper, EmployeeApi employeeApi) {
        return new NetworkAccessHelperImpl(dbHelper, employeeApi);
    }
}
