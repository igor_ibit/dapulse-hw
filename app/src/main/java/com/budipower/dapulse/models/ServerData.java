package com.budipower.dapulse.models;

import java.util.List;

/**
 * Created by igori on 03/03/2017.
 */

public class ServerData {
    private List<Employee> employees;

    public List<Employee> getEmployees() {
        return employees;
    }
}
