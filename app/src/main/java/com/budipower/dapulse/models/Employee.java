package com.budipower.dapulse.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

/**
 * Created by igori on 03/03/2017.
 */
@RealmClass
public class Employee implements RealmModel
{
    private int id;

    private String name;

    private String phone;

    private String email;

    private String title;

    @SerializedName("profile_pic")
    private String profileImage;

    @SerializedName("manager_id")
    private int managerId;

    private String department;

    @SerializedName("is_manager")
    private boolean isManager;


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getTitle() {
        return title;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public int getManagerId() {
        return managerId;
    }

    public String getDepartment() {
        return department;
    }

    public boolean isManager() {
        return isManager;
    }
}
