package com.budipower.dapulse;

import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.budipower.dapulse.models.Employee;
import com.budipower.dapulse.utils.ImageUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by igori on 03/03/2017.
 */

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.EmployeeViewHolder> {

    private OnItemClickedListener onItemClickedListener;
    private String imageTransitionName;
    private List<Employee> data = new ArrayList<>();

    public EmployeeAdapter(OnItemClickedListener onItemClickedListener, String imageTransitionName) {
        this.onItemClickedListener = onItemClickedListener;
        this.imageTransitionName = imageTransitionName;
    }

    public void setData(Collection<Employee> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EmployeeViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.employee_layout, parent, false),
                onItemClickedListener,
                imageTransitionName);
    }

    @Override
    public void onBindViewHolder(EmployeeViewHolder holder, int position) {
        holder.bind(data.get(holder.getAdapterPosition()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class EmployeeViewHolder extends RecyclerView.ViewHolder {

        private final OnItemClickedListener onItemClickedListener;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.title)
        TextView title;

        EmployeeViewHolder(View itemView, OnItemClickedListener onItemClickedListener, String imageTransitionName) {
            super(itemView);
            this.onItemClickedListener = onItemClickedListener;
            ButterKnife.bind(this, itemView);
            image.setTransitionName(imageTransitionName);
        }

        void bind(final Employee employee) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickedListener.onItemClicked(employee.getId(),
                            Pair.create(image, image.getTransitionName()));
                }
            });
            ImageUtils.loadCircularImage(image, employee.getProfileImage());
            name.setText(employee.getName());
            title.setText(employee.getTitle());
        }
    }

    public interface OnItemClickedListener {
        void onItemClicked(int id, Object transition);
    }
}
