package com.budipower.dapulse;

import android.app.Application;

import com.budipower.dapulse.di.ApiModule;
import com.budipower.dapulse.di.AppComponent;
import com.budipower.dapulse.di.DaggerAppComponent;
import com.budipower.dapulse.di.DataModule;
import com.budipower.dapulse.di.HelpersModule;

/**
 * Created by igori on 03/03/2017.
 */

public class App extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .apiModule(new ApiModule())
                .dataModule(new DataModule())
                .helpersModule(new HelpersModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
