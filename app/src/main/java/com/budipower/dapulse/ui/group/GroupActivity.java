package com.budipower.dapulse.ui.group;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.budipower.dapulse.App;
import com.budipower.dapulse.EmployeeAdapter;
import com.budipower.dapulse.R;
import com.budipower.dapulse.data.DataProvider;
import com.budipower.dapulse.di.group.GroupActivityModule;
import com.budipower.dapulse.models.Employee;
import com.budipower.dapulse.ui.employee.EmployeeActivity;
import com.budipower.dapulse.utils.ImageUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupActivity extends AppCompatActivity implements GroupView {

    private static final String KEY_MANAGER_ID = "keyManager";
    public static String ImageTransitionName = "managerImage";

    @Inject
    DataProvider dataProvider;
    @Inject
    GroupPresenter presenter;

    @BindView(R.id.groupName)
    TextView groupName;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.recycler)
    RecyclerView recycler;


    private EmployeeAdapter adapter;
    private int managerId;

    @SuppressWarnings("unchecked")
    public static void start(Activity activity, int managerId, Object transition) {
        Intent intent = new Intent(activity, GroupActivity.class);
        intent.putExtra(KEY_MANAGER_ID, managerId);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, (Pair<View, String>) transition);
        activity.startActivity(intent,options.toBundle());
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        ButterKnife.bind(this);

        ((App) getApplicationContext()).getAppComponent()
                .plus(new GroupActivityModule(this))
                .inject(this);

        managerId = getIntent().getIntExtra(KEY_MANAGER_ID, 0);
        if (managerId == 0) {
            finish();
            return;
        }

        recycler.setLayoutManager(new GridLayoutManager(this, 3));
        adapter = new EmployeeAdapter(new EmployeeAdapter.OnItemClickedListener() {
            @Override
            public void onItemClicked(int id, Object transition) {
                presenter.onEmployeeClicked(id, transition);
            }
        }, EmployeeActivity.ImageTransitionName);
        recycler.setAdapter(adapter);

        presenter.onCreate();
    }

    @Override
    public int getDesignatedId() {
        return managerId;
    }

    @Override
    public void populateEmployees(List<Employee> employees) {
        adapter.setData(employees);
    }

    @Override
    public void setGroupName(String department) {
        groupName.setText(department);
    }

    @Override
    public void setManagerName(String name) {
        this.name.setText(name);
    }

    @Override
    public void setManagerTitle(String title) {
        this.title.setText(title);
    }

    @Override
    public void setImage(String url) {
        ImageUtils.loadCircularImage(image, url);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onEmployeeClicked(managerId, Pair.create(image, image.getTransitionName()));
            }
        });
    }

    @Override
    public void openEmployeeActivity(int id, Object transition) {
        EmployeeActivity.start(this, id, transition);
    }
}
