package com.budipower.dapulse.ui.group;

import com.budipower.dapulse.models.Employee;

import java.util.List;

/**
 * Created by igori on 03/03/2017.
 */

interface GroupView {
    int getDesignatedId();

    void populateEmployees(List<Employee> employees);

    void setGroupName(String department);

    void setManagerName(String name);

    void setManagerTitle(String title);

    void setImage(String url);

    void openEmployeeActivity(int id, Object transitions);
}
