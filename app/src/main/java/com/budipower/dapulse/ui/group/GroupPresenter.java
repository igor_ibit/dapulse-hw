package com.budipower.dapulse.ui.group;

import com.budipower.dapulse.data.LifeCycleEvents;

/**
 * Created by igori on 03/03/2017.
 */

public interface GroupPresenter extends LifeCycleEvents{

    void onEmployeeClicked(int id, Object transitions);
}
