package com.budipower.dapulse.ui.group;

import com.budipower.dapulse.data.DataProvider;
import com.budipower.dapulse.models.Employee;

import java.util.List;

/**
 * Created by igori on 03/03/2017.
 */

public class GroupPresenterImpl implements GroupPresenter {
    private final GroupView groupView;
    private final DataProvider dataProvider;

    public GroupPresenterImpl(GroupView groupView, DataProvider dataProvider) {

        this.groupView = groupView;
        this.dataProvider = dataProvider;
    }

    @Override
    public void onCreate() {
        int id = groupView.getDesignatedId();
        Employee manager = dataProvider.get(id);
        List<Employee> employees = dataProvider.getEmployees(manager.getId());
        groupView.populateEmployees(employees);
        groupView.setGroupName(manager.getDepartment());
        groupView.setManagerName(manager.getName());
        groupView.setManagerTitle(manager.getTitle());
        groupView.setImage(manager.getProfileImage());
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onEmployeeClicked(int id, Object transition) {
        groupView.openEmployeeActivity(id, transition);
    }
}
