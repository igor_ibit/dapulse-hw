package com.budipower.dapulse.ui.employee;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.budipower.dapulse.App;
import com.budipower.dapulse.R;
import com.budipower.dapulse.data.DataProvider;
import com.budipower.dapulse.di.EmployeeActivityModule;
import com.budipower.dapulse.utils.ImageUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmployeeActivity extends AppCompatActivity implements EmployeeView {

    private static final String KEY_ID = "keyid";
    public static String ImageTransitionName = "employeeImage";

    @Inject
    DataProvider dataProvider;
    @Inject
    EmployeePresenter presenter;

    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.email)
    TextView email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee);
        ButterKnife.bind(this);

        ((App) getApplicationContext()).getAppComponent()
                .plus(new EmployeeActivityModule(this))
                .inject(this);

        int id = getIntent().getIntExtra(KEY_ID, 0);
        if (id == 0) {
            finish();
            return;
        }

        presenter = new EmployeePresenterImpl(this, dataProvider);
        presenter.onCreate();
    }

    @SuppressWarnings("unchecked")
    public static void start(Activity activity, int employeeId, Object transition) {
        Intent intent = new Intent(activity, EmployeeActivity.class);
        intent.putExtra(KEY_ID, employeeId);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                activity,
                (Pair<View, String>) transition);
        activity.startActivity(intent, options.toBundle());
    }

    @Override
    public int getDesignatedId() {
        return getIntent().getIntExtra(KEY_ID, 0);
    }

    @Override
    public void setName(String name) {
        this.name.setText(name);
    }

    @Override
    public void setImage(String profileImage) {
        ImageUtils.loadCircularImage(image, profileImage);
    }

    @Override
    public void setTitle(String title) {
        this.title.setText(title);
    }

    @Override
    public void setPhone(String phone) {
        this.phone.setText(String.format(getString(R.string.phone_format), phone.split("x")[0]));
    }

    @Override
    public void setEmail(String email) {
        this.email.setText(String.format(getString(R.string.email_fromat), email));
    }
}
