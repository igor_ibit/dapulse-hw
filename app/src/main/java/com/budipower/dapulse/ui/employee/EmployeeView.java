package com.budipower.dapulse.ui.employee;

/**
 * Created by igori on 03/03/2017.
 */

interface EmployeeView {
    int getDesignatedId();

    void setName(String name);

    void setImage(String profileImage);

    void setTitle(String title);

    void setPhone(String phone);

    void setEmail(String email);
}
