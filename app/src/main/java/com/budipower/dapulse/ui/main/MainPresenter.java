package com.budipower.dapulse.ui.main;

import com.budipower.dapulse.data.LifeCycleEvents;

/**
 * Created by igori on 03/03/2017.
 */

public interface MainPresenter extends LifeCycleEvents {

    void onEmployeeClicked(int id, Object transitions);
}
