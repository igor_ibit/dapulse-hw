package com.budipower.dapulse.ui.main;

import com.budipower.dapulse.data.DataProvider;

/**
 * Created by igori on 03/03/2017.
 */

public class MainPresenterImpl implements MainPresenter {

    private DataProvider dataProvider;
    private final MainView mainView;


    public MainPresenterImpl(DataProvider dataProvider, MainView mainView) {
        this.dataProvider = dataProvider;
        this.mainView = mainView;
    }

    @Override
    public void onCreate() {
        dataProvider.init(new DataProvider.OnDataReadyListener() {
            @Override
            public void onDataReady() {
                mainView.populateEmployees(dataProvider.getManagers());
            }
        });
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }



    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onEmployeeClicked(int id, Object transition) {
        mainView.openManagerActivity(id, transition);
    }
}
