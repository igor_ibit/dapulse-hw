package com.budipower.dapulse.ui.employee;

import com.budipower.dapulse.data.DataProvider;
import com.budipower.dapulse.models.Employee;

/**
 * Created by igori on 03/03/2017.
 */

public class EmployeePresenterImpl implements EmployeePresenter {

    private EmployeeView employeeView;
    private DataProvider dataProvider;

    public EmployeePresenterImpl(EmployeeView employeeView, DataProvider dataProvider) {
        this.employeeView = employeeView;
        this.dataProvider = dataProvider;
    }


    @Override
    public void onCreate() {
        int id = employeeView.getDesignatedId();
        Employee employee = dataProvider.get(id);
        employeeView.setImage(employee.getProfileImage());
        employeeView.setName(employee.getName());
        employeeView.setTitle(employee.getTitle());
        employeeView.setPhone(employee.getPhone());
        employeeView.setEmail(employee.getEmail());
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }
}
