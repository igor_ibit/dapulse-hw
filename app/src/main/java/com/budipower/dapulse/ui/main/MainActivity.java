package com.budipower.dapulse.ui.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.budipower.dapulse.App;
import com.budipower.dapulse.EmployeeAdapter;
import com.budipower.dapulse.R;
import com.budipower.dapulse.data.DataProvider;
import com.budipower.dapulse.di.main.MainActivityModule;
import com.budipower.dapulse.models.Employee;
import com.budipower.dapulse.ui.group.GroupActivity;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainView {

    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.progress)
    AVLoadingIndicatorView progress;
    private EmployeeAdapter adapter;

    @Inject
    DataProvider dataProvider;

    @Inject
    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ((App) getApplicationContext()).getAppComponent()
                .plus(new MainActivityModule(this))
                .inject(this);

        recycler.setLayoutManager(new GridLayoutManager(this, 3));
        adapter = new EmployeeAdapter(new EmployeeAdapter.OnItemClickedListener() {
            @Override
            public void onItemClicked(int id, Object transition) {
                presenter.onEmployeeClicked(id, transition);
            }
        }, GroupActivity.ImageTransitionName);
        recycler.setAdapter(adapter);
//        presenter = new MainPresenterImpl(dataProvider, this);
        presenter.onCreate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void populateEmployees(List<Employee> managers) {
        progress.hide();
        adapter.setData(managers);
    }

    @Override
    public void openManagerActivity(int id, Object transition) {
        GroupActivity.start(this, id, transition);
    }
}
