package com.budipower.dapulse.ui.main;

import com.budipower.dapulse.models.Employee;

import java.util.List;

/**
 * Created by igori on 03/03/2017.
 */

public interface MainView {

    void populateEmployees(List<Employee> managers);

    void openManagerActivity(int id, Object transitions);
}
