package com.budipower.dapulse.data;

/**
 * Created by igori on 03/03/2017.
 */

public interface LifeCycleEvents {

    void onCreate();
    void onStart();
    void onResume();
    void onPause();
    void onStop();
    void onDestroy();
}
