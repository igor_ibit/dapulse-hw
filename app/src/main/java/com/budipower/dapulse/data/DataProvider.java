package com.budipower.dapulse.data;

import com.budipower.dapulse.models.Employee;

import java.util.List;

/**
 * Created by igori on 03/03/2017.
 */

public interface DataProvider {

    void init(DataProvider.OnDataReadyListener listener);

    List<Employee> getManagers();

    List<Employee> getEmployees(int managerId);

    Employee get(int id);

    interface OnDataReadyListener {
        void onDataReady();
    }
}
