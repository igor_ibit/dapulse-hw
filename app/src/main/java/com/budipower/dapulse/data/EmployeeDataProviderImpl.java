package com.budipower.dapulse.data;

import com.budipower.dapulse.helpers.DBHelper;
import com.budipower.dapulse.helpers.NetworkAccessHelper;
import com.budipower.dapulse.models.Employee;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by igori on 03/03/2017.
 */

public class EmployeeDataProviderImpl implements DataProvider {

    private DBHelper dbHelper;
    private NetworkAccessHelper networkHelper;

    @Inject
    public EmployeeDataProviderImpl(DBHelper dbHelper, NetworkAccessHelper networkHelper) {
        this.dbHelper = dbHelper;
        this.networkHelper = networkHelper;
    }

    @Override
    public void init(OnDataReadyListener listener) {
        if (dbHelper.isLoaded()) {
            listener.onDataReady();
        }

        networkHelper.load(listener);
    }

    @Override
    public List<Employee> getManagers() {
        return dbHelper.getManagers();
    }

    @Override
    public List<Employee> getEmployees(int managerId) {
        return dbHelper.getEmployees(managerId);
    }

    @Override
    public Employee get(int id) {
        return dbHelper.getById(id);
    }
}
