package com.budipower.dapulse.helpers;

import com.budipower.dapulse.data.DataProvider;
import com.budipower.dapulse.models.ServerData;
import com.budipower.dapulse.retrofit.EmployeeApi;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by igori on 03/03/2017.
 */

@Singleton
public class NetworkAccessHelperImpl implements NetworkAccessHelper {

    private DBHelper dbHelper;
    private EmployeeApi employeeApi;

    @Inject
    public NetworkAccessHelperImpl(DBHelper dbHelper, EmployeeApi employeeApi) {
        this.dbHelper = dbHelper;
        this.employeeApi = employeeApi;
    }

    @Override
    public void load(final DataProvider.OnDataReadyListener listener) {
        Call<ServerData> call = employeeApi.getEmployees();

        call.enqueue(new Callback<ServerData>() {
            @Override
            public void onResponse(Call<ServerData> call, Response<ServerData> response) {
                dbHelper.insertAll(response.body().getEmployees());
                listener.onDataReady();
            }

            @Override
            public void onFailure(Call<ServerData> call, Throwable t) {
            }
        });

    }
}
