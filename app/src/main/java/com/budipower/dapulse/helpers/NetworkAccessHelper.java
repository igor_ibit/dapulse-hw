package com.budipower.dapulse.helpers;

import com.budipower.dapulse.data.DataProvider;

/**
 * Created by igori on 03/03/2017.
 */

public interface NetworkAccessHelper {
    void load(DataProvider.OnDataReadyListener listener);
}
