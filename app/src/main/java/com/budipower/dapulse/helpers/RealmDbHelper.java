package com.budipower.dapulse.helpers;

import com.budipower.dapulse.models.Employee;

import java.util.List;

import io.realm.Realm;

/**
 * Created by igori on 03/03/2017.
 */

public class RealmDbHelper implements DBHelper {

    private Realm realm = Realm.getDefaultInstance();

    @Override
    public boolean isLoaded() {
        return !realm.isEmpty();
    }

    @Override
    public Employee getById(int id) {
        return realm.where(Employee.class).equalTo("id", id).findFirst();
    }

    @Override
    public void insertAll(List<Employee> list) {
        realm.beginTransaction();
        realm.delete(Employee.class);
        realm.copyToRealm(list);
        realm.commitTransaction();
    }

    @Override
    public List<Employee> getManagers() {
        return realm.copyFromRealm(realm.where(Employee.class).equalTo("isManager", true).findAll());
    }

    @Override
    public List<Employee> getEmployees(int managerId) {
        return realm.copyFromRealm(realm.where(Employee.class).equalTo("managerId", managerId).findAll());
    }
}
