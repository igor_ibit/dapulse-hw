package com.budipower.dapulse.helpers;

import com.budipower.dapulse.models.Employee;

import java.util.List;

/**
 * Created by igori on 03/03/2017.
 */

public interface DBHelper {

    boolean isLoaded();

    Employee getById(int id);

    void insertAll(List<Employee> list);

    List<Employee> getManagers();

    List<Employee> getEmployees(int managerId);
}
