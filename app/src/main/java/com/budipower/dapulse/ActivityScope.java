package com.budipower.dapulse;

import javax.inject.Scope;

/**
 * Created by igori on 03/03/2017.
 */

@Scope
public @interface ActivityScope {
}
