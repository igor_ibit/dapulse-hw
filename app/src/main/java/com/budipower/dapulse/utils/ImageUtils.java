package com.budipower.dapulse.utils;

import android.widget.ImageView;

import com.budipower.dapulse.R;
import com.squareup.picasso.Picasso;

/**
 * Created by igori on 04/03/2017.
 */

public class ImageUtils {
    public static void loadCircularImage(ImageView image, String url) {
        Picasso.with(image.getContext())
                .load(url)
                .transform(new CircleTransformation())
                .placeholder(R.mipmap.contact_placeholder)
                .noFade()
                .into(image);
    }
}
