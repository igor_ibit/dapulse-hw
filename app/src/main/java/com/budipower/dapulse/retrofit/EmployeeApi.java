package com.budipower.dapulse.retrofit;

import com.budipower.dapulse.models.ServerData;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by igori on 03/03/2017.
 */

public interface EmployeeApi {

    @GET("/")
    Call<ServerData> getEmployees();
}
